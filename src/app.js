'use strict';

// requirements
const express = require('express');
const MAX_NUMBER=2147483647;
const MAX_HTTP_POST_SIZE=1*1024;
// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

// Main 
app.get('/', (req, res) => { 
    if(Object.keys(req.body).length>MAX_HTTP_POST_SIZE){
        throw new Error('Req size too big');
    }
    if(!req.query.amount){
        throw new Error('amount cannot be null')
    }
    if(Number.isInteger(req.query.amount)){
        throw new RangeError();
    }
    if (approval(parseInt(req.query.amount))) { 
        res.status(400).end(req.query.amount + ' requires approval.');
    } else {
        res.status(200).end(req.query.amount + ' does not require approval.');
    }
});

// Transaction approval
// If an amount is less than a threashold
// approval is not required
var approval = (value) => {
    var threashold = 1000;
    var surcharge = 10;
    if(value<0 || value >(MAX_NUMBER-surcharge)){
        throw new RangeError();
    }
    var amount = parseInt(value+surcharge);
    //var amount = parseInt(value) + surcharge;
    console.log(amount);
    if (amount >= threashold) {
        return true;
    };
    return false;
};

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = { app, approval };
